# chatbot_watson_install_onewex
Ansible playbooks for installing CHATBOT.
 
# Roles
Below are the roles used in this playbook

* chatbot_watson_install_onewex -- Installs the prerequisites and Watson Explorer oneWEX application (CHATBOT) using this playbook.

# Getting Started

* Create the Directory structures required for DOCKER & WATSON installation.
* Mounts the required filesystems.
* Create the soft link in linux for complying the DOCKER & WATSONN specific directory structure.
* Installs & configures Docker and it storage.
* Installs & configures Watson Explorer oneWEX.
 
## Running Playbook
ansible-playbooks -i inventories -e __host_group_all=chatbot chatbot_install.yml

### Parameters to be modified
* run_as - User to run the playbook
* __chatbot_user_name == Owner for the chatbot files and binaries
* __chatbot_group_name == Group owner for chatbot files and binaries
* docker_user_name - Docker user name
* docker_group_name - Docker group name
* __chatbot_repo_url: The URL for Chatbot binaries.
* __chatbot_repo_username: User name for chatbot repo
* __chatbot_repo_password: Password for chatbot repo

#### Chatbot uninstallation

Run the following playbook to uninstall datarobot

ansible-playbook -i inventories -e __host_group_all=chatbot chatbot-uninstall.yml 